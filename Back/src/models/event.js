const mongoose = require('../database');
const Scheme = mongoose.Schema;

const eventSchema = new Scheme({
title:{
    type: String,
    require:true
},
start:{
    type:Date,
    require:true,
},
end:{
    type:Date,
    require:false,
},
url:{
    type: String,
    require:false,
},
backgroundColor:{
    type:String,
    require:false
},
createdAt:{
    type: Date,
    default: Date.now,
}
});

const Event = mongoose.model('Event', eventSchema);

module.exports = Event;