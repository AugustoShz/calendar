require('dotenv/config');
const mongoose = require('mongoose');
const url = process.env.DB_PATH

mongoose.connect(url,{useNewUrlParser:true, useUnifiedTopology:true, useFindAndModify:false});
mongoose.Promise = global.Promise;


module.exports = mongoose