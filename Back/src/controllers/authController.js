const express = require('express');
const Event = require('../models/event');
const router = express.Router();

router.post('/create', async(req,res)=>{
    try{
        const event = await Event.create(req.body);
        console.log(req.body)
        return res.send({event});
    }catch(err){
        return res.status(400).send({error: "Error creating event", err: err});
    };
});
router.get('/', async (req, res) => {
    try {
        const events = await Event.find();

        return res.send({ events })
    } catch (err) {
        return res.status(400).send({error: "Error loading events"});
    }
})

router.put('/:eventId', async (req, res) => {
    try {
        const event = await Event.updateOne({_id:req.params.eventId},req.body,{new: true});
        console.log(req.body)
        return res.send({event});
    } catch (err) {
        return res.status(400).send({error: "Error updating event"});
    }
})
router.delete('/delete/:id', async(req,res)=>{
    try{
        const event = await Event.deleteOne({_id:req.params.id});
        if(event.deletedCount == 0){
            throw "NoEventFound"
        }
        return res.status(200).send();
    }catch(err){
        return res.status(400).send({error: err});
    }
})
module.exports = app => app.use('/op', router);