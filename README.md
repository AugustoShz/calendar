# Calendar

## Sobre
Este projeto consiste em um calendário utilizando FullCalendar.io, VueJS, NodeJS e MongoDB.

## Importante
É importante abrir o back-end antes do fron-end!
O projeto esta sendo rodado localmente na maquina.
A API roda na porta 3000

É requisito obrigatório NodeJS e o gerenciador de pacotes npm

Podem ser necontrados para download no seguinte link:
```
https://nodejs.org/en/
```

Para ligar o projeto, siga as instruções a seguir:

## Instalar pacotes
Existem 2 pastas nomeadas como "Front" e "Back", acesse as pastas pelo terminal e execute o seguinte comando em ambas:

```
npm install
```

## Iniciar o Back
No terminal, dentro da pasta "Back", utilize o comando: 

```
node ./src/index.js
```

O Back-End utiliza .env para acessar o servidor do mongoDB, crie o arquivo e coloque o url do seu banco de dados nele com o noem de variavel "DB_PATH"
Exemplo:
```
DB_PATH=mongodb://localhost/test
```

## Iniciar o Front
Novamente, tenha certeza que o back esta rodando.
No terminal, dentro da pasta "Front", utilize o comando:

```
npm run serve
```