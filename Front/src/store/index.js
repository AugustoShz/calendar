import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuex, axios, VueAxios)
var baseUrl = "http://localhost:3000"

export default new Vuex.Store({
  state: {
    events: []
  },
  mutations: {
    SAVE_EVENTS(state, events){
      events.events.forEach(element => {
        element.id = element._id
      });
      state.events = events

    }
  },
  actions: {
    loadEvents({commit}) {
      axios.get(baseUrl+"/op")
      .then(res=>{
        commit('SAVE_EVENTS', res.data);
      })
    },
    createEvent({dispatch},payload){
      console.log(payload)
      axios.post(baseUrl+"/op/create/", payload)
      .then(res =>{
        console.log(res)
        dispatch("loadEvents")
      })
      .catch(err => {
        console.log(err)
      })
    },
    editEvent({dispatch},payload){
      console.log(payload)
      axios.put(baseUrl+"/op/"+payload.eventId, payload.event)
      .then(res =>{
        console.log(res)
        dispatch("loadEvents")
      })
      .catch(err => {
        console.log(err)
      })
    },
    deleteEvent({dispatch},payload){
      console.log(payload)
      axios.delete(baseUrl+"/op/delete/"+payload.eventId)
      .then(res=>{
        console.log(res)
        dispatch("loadEvents")
      })
      .catch(err=>{
        console.log(err)
      })
    }
  },
  modules: {
  }
})
